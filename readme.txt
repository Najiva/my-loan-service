Example of usage:

Simple loan request (POST):

URL: http://localhost:8081/loan/request

BODY:
{
	"provenIncome" : 65000.0,
	"requestedAmount" : 2000000.0,
	"savingsAmount" : 50000.0,
	"requestedTerm": "P20Y",
	"customer" : {
		"customerId": 1	
	}
}

What is missing:

1) I did not implement following requirement "Client should be able to retrieve full list of loans history"

2) There is no service for resetting of the IP requests counter. It should reset at midnight.

3) There is no security/authentication mechanism implemented.
