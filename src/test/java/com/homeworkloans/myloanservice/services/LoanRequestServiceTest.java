package com.homeworkloans.myloanservice.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.Period;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.homeworkloans.myloanservice.IpCounter;
import com.homeworkloans.myloanservice.entities.Customer;
import com.homeworkloans.myloanservice.entities.LoanRequest;
import com.homeworkloans.myloanservice.enums.LoanRequestResult;
import com.homeworkloans.myloanservice.repositires.LoanRepository;
import com.homeworkloans.myloanservice.repositires.LoanRequestRepository;

@RunWith(SpringRunner.class)
public class LoanRequestServiceTest {

    @TestConfiguration
    static class LoanRequestServiceTestConfiguration {

        @Bean
        public LoanRequestService getLoanRequestService() {
            return new LoanRequestService();
        }
    }

    @Autowired
    private LoanRequestService instance;

    @MockBean
    private LoanRepository loanRepoMock;
    @MockBean
    private LoanRequestRepository loanReqRepoMock;
    @MockBean
    private IpCounter ipCounterMock;
    @MockBean
    private LoanRequest loanRequestMock;

    @BeforeClass
    public static void setup() {
    }

    @Test
    public void testEvaluateLoanRequest_tooManyCalls() {

        // Testing objects 
        Customer customer = new Customer(4L, "Jan", "Hus");
        LoanRequest request = new LoanRequest();
        request.setDateRequested(LocalDateTime.now());
        request.setProvenIncome(100000.0);
        request.setRequestedAmount(2000000.0);
        request.setSavingsAmount(300000.0);
        request.setRequestedTerm(Period.ofYears(20));
        request.setCustomer(customer);

        // Configure mock IpCounter
        Mockito.when(ipCounterMock.callCountReached(anyString())).thenReturn(true);

        // Call method
        LoanRequestResult result = instance.performRiskAnalysis(request, "any IP address");

        // Assertions
        assertThat(result).isEqualTo(LoanRequestResult.TOO_MANY_REQ_FROM_SINGLE_IP);
    }

    @Test
    public void testEvaluateLoanRequest_notCorrectTime() {

        // Test time within 0 and 6 AM (1:00 AM)
        LocalDateTime morningDateTime = LocalDateTime.of(
                LocalDate.of(2018, Month.JANUARY, 1),
                LocalTime.of(1, 0, 0, 0)
        );

        // Configure loan request
        Mockito.when(loanRequestMock.getDateRequested()).thenReturn(morningDateTime);

        // Call method
        LoanRequestResult result = instance.performRiskAnalysis(loanRequestMock, "any IP address");

        // Assertions
        assertThat(result).isEqualTo(LoanRequestResult.REQUEST_NOT_WITHIN_REQUIRED_TIME);
    }

    @Test
    public void testEvaluateLoanRequest_preapprove() {

        // Test time within 0 and 6 AM (1:00 AM)
        LocalDateTime afternoonDateTime = LocalDateTime.of(
                LocalDate.of(2018, Month.JANUARY, 1),
                LocalTime.of(10, 0, 0, 0)
        );

        // Configure loan request
        Mockito.when(loanRequestMock.getDateRequested()).thenReturn(afternoonDateTime);

        // Call method
        LoanRequestResult result = instance.performRiskAnalysis(loanRequestMock, "any IP address");

        // Assertions
        assertThat(result).isEqualTo(LoanRequestResult.LOAN_REQUEST_ACCEPTED);
    }

}
