package com.homeworkloans.myloanservice.entities;

import java.time.LocalDateTime;
import java.time.Period;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.homeworkloans.myloanservice.enums.LoanRequestState;
import java.io.Serializable;
import javax.persistence.GenerationType;

@Entity
@Table(name = "loan_requests")
public class LoanRequest implements Serializable {

    private Long requestId;
    private LocalDateTime dateRequested;
    private Double provenIncome;
    private Double requestedAmount;
    private Double savingsAmount;
    private Period requestedTerm;
    private LoanRequestState requestResult;
    private Customer customer;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, nullable = false)
    public Long getRequestId() {
        return requestId;
    }

    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }

    @Column(name = "date_requested", nullable = false)
    public LocalDateTime getDateRequested() {
        return dateRequested;
    }

    public void setDateRequested(LocalDateTime dateRequested) {
        this.dateRequested = dateRequested;
    }

    @Column(name = "proven_income", nullable = false)
    public Double getProvenIncome() {
        return provenIncome;
    }

    public void setProvenIncome(Double provenIncome) {
        this.provenIncome = provenIncome;
    }

    @Column(name = "requested_amount", nullable = false)
    public Double getRequestedAmount() {
        return requestedAmount;
    }

    public void setRequestedAmount(Double requestedAmount) {
        this.requestedAmount = requestedAmount;
    }

    @Column(name = "savings_amount", nullable = false)
    public Double getSavingsAmount() {
        return savingsAmount;
    }

    public void setSavingsAmount(Double savingsAmount) {
        this.savingsAmount = savingsAmount;
    }

    @Column(name = "requested_term", nullable = false)
    public Period getRequestedTerm() {
        return requestedTerm;
    }

    public void setRequestedTerm(Period term) {
        this.requestedTerm = term;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "request_result")
    public LoanRequestState getRequestResult() {
        return requestResult;
    }

    public void setRequestResult(LoanRequestState requestResult) {
        this.requestResult = requestResult;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customerId", nullable = false)
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

}
