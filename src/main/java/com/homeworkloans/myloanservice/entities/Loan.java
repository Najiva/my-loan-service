package com.homeworkloans.myloanservice.entities;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.homeworkloans.myloanservice.enums.LoanState;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.GenerationType;

@Entity
@Table(name = "loans")
public class Loan implements Serializable {

    private Long loanId;
    private Customer customer;
    private Double amount;
    private LoanState state;

    public Loan() {
    }

    public Loan(Customer customer, Double amount, LoanState state) {
        super();
        this.customer = customer;
        this.amount = amount;
        this.state = state;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, nullable = false)
    public Long getLoanId() {
        return loanId;
    }

    public void setLoanId(Long loanId) {
        this.loanId = loanId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customerId", nullable = false)
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amountApproved) {
        this.amount = amountApproved;
    }

    @Enumerated(EnumType.STRING)
    public LoanState getState() {
        return state;
    }

    public void setState(LoanState state) {
        this.state = state;
    }

}
