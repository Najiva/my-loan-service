package com.homeworkloans.myloanservice.constants;

public final class Constants {

	// Maximal number of requests from single IP per day
	public static final Integer MAX_NUMBER_OF_REQUESTS = 3;
	
	// ENG
	public static final String INVALID_REQUIRED_TIME = "Sorry the request must be submitted between 00:00 and 06:00 AM";
	public static final String TOO_MANY_REQ_FROM_IP = "Sorry you have reached maximum number of requests per day from a single IP address";
	public static final String SUCCESS_MSG = "Congratulations, the requested loan has been pre-approved. Its reference number is: ";
	public static final String INVALID_EVAL_RESULT = "Invalid request evaluation result.";

}
