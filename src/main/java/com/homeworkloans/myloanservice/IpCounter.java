package com.homeworkloans.myloanservice;

import java.util.HashMap;
import java.util.Map;

import com.homeworkloans.myloanservice.constants.Constants;

public class IpCounter {

    private Map<String, Integer> ipCounter = new HashMap<String, Integer>();

    public Boolean callCountReached(String ip) {

        Boolean callCountReached = true;

        Integer count = ipCounter.get(ip);
        if (count == null) {
            ipCounter.put(ip, 1);
            callCountReached = false;
        } else if (count < Constants.MAX_NUMBER_OF_REQUESTS) {
            ipCounter.put(ip, count + 1);
            callCountReached = false;
        }

        return callCountReached;

    }

    @Override
    public String toString() {
        return "IpCounter [ipCounter=" + ipCounter + "]";
    }

}
