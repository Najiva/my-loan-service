/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.homeworkloans.myloanservice.services;

import com.homeworkloans.myloanservice.entities.Customer;
import com.homeworkloans.myloanservice.repositires.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Juraj
 */
@Service
public class CustomerService {
    
    @Autowired
    private CustomerRepository customerRepo;
    
    public Customer load(Long id){
        System.out.println("Custromer ID: " + Long.toString(id));
        return customerRepo.findById(id).get();
    }
}
