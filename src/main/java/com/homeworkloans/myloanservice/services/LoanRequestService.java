package com.homeworkloans.myloanservice.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.homeworkloans.myloanservice.IpCounter;
import com.homeworkloans.myloanservice.entities.LoanRequest;
import com.homeworkloans.myloanservice.enums.LoanRequestResult;
import com.homeworkloans.myloanservice.repositires.LoanRequestRepository;

@Service
public class LoanRequestService {

	@Autowired
	private IpCounter ipCounter;

	@Autowired
	private LoanRequestRepository loanRequestRepo;

	public LoanRequestResult performRiskAnalysis(LoanRequest request, String requestIp) {

		int requestHour = request.getDateRequested().getHour();

		if ((requestHour > 0) && (requestHour < 6)) {
			
			return LoanRequestResult.REQUEST_NOT_WITHIN_REQUIRED_TIME;

		} else if (ipCounter.callCountReached(requestIp)) {
			
			return LoanRequestResult.TOO_MANY_REQ_FROM_SINGLE_IP;
			
		} else {

			return LoanRequestResult.LOAN_REQUEST_ACCEPTED;
		}

	}

	public void create(LoanRequest request) {
		loanRequestRepo.save(request);
	}

}
