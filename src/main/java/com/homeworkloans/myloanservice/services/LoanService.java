package com.homeworkloans.myloanservice.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.homeworkloans.myloanservice.entities.Loan;
import com.homeworkloans.myloanservice.repositires.LoanRepository;

@Service
public class LoanService {

	@Autowired
	private LoanRepository loanRepo;
	
	public void create(Loan loan) {
		loanRepo.save(loan);
	}
	
}
