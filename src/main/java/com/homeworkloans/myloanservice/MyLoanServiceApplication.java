package com.homeworkloans.myloanservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class MyLoanServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyLoanServiceApplication.class, args);
	}

}
