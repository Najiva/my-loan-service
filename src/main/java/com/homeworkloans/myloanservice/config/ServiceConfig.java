package com.homeworkloans.myloanservice.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.homeworkloans.myloanservice.IpCounter;

@Configuration
public class ServiceConfig {

	@Bean
	public IpCounter getIpCounter() {
		return new IpCounter();
	}
	
}
