package com.homeworkloans.myloanservice.controllers;

import java.time.LocalDateTime;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.homeworkloans.myloanservice.constants.Constants;
import com.homeworkloans.myloanservice.entities.Customer;
import com.homeworkloans.myloanservice.entities.Loan;
import com.homeworkloans.myloanservice.entities.LoanRequest;
import com.homeworkloans.myloanservice.enums.LoanRequestResult;
import com.homeworkloans.myloanservice.enums.LoanRequestState;
import com.homeworkloans.myloanservice.enums.LoanState;
import com.homeworkloans.myloanservice.services.CustomerService;
import com.homeworkloans.myloanservice.services.LoanRequestService;
import com.homeworkloans.myloanservice.services.LoanService;

@RestController
@RequestMapping("/loan")
public class LoanRequestController {
	
	@Autowired
	LoanRequestService requestService;
	
	@Autowired
	LoanService loanService;
        
        @Autowired 
        CustomerService customerService;
	
	@PostMapping("/request")
	public String evaluateRequest(
			@RequestBody LoanRequest loanRequest, 
			HttpServletRequest request) {
		
		String response = "";
		
		// Find out when the request was made (server time)
		LocalDateTime reqDateTime = LocalDateTime.now();
		
		// Set request date and time in loan request object
		loanRequest.setDateRequested(reqDateTime);
		
		// Evaluate the request
		LoanRequestResult result = requestService.performRiskAnalysis(loanRequest, request.getRemoteAddr());
		
		switch (result) {
		case REQUEST_NOT_WITHIN_REQUIRED_TIME:
			loanRequest.setRequestResult(LoanRequestState.REJECTED);
			response = Constants.INVALID_REQUIRED_TIME;
			break;
			
		case TOO_MANY_REQ_FROM_SINGLE_IP:
			loanRequest.setRequestResult(LoanRequestState.REJECTED);
			response = Constants.TOO_MANY_REQ_FROM_IP;
			break;
			
		case LOAN_REQUEST_ACCEPTED:
			loanRequest.setRequestResult(LoanRequestState.ACCEPTED);
                        Customer customer = customerService.load(loanRequest.getCustomer().getCustomerId());
                        // Create the loan
			Loan loan = new Loan(customer, loanRequest.getRequestedAmount(), LoanState.PREAPPROVED);
			// Persist the loan object
                        loanService.create(loan);
			response = Constants.SUCCESS_MSG + loan.getLoanId();
			break;
			
		default:
			loanRequest.setRequestResult(LoanRequestState.REJECTED);
			response = Constants.INVALID_EVAL_RESULT;
		}
		
		// Now persist also the request
		requestService.create(loanRequest);
		
		return response;

	}
	
}
