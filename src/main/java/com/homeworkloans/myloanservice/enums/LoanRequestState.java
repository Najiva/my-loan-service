package com.homeworkloans.myloanservice.enums;

public enum LoanRequestState {

	NONEVALUATED,
	ACCEPTED,
	REJECTED
	
}
