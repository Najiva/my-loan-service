package com.homeworkloans.myloanservice.enums;

public enum LoanState {

	PREAPPROVED,
	APPROVED,
	ACTIVE,
	FINISHED
	
}
