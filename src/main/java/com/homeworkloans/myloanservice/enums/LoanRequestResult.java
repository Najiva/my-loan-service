package com.homeworkloans.myloanservice.enums;

public enum LoanRequestResult {

	REQUEST_NOT_WITHIN_REQUIRED_TIME,
	TOO_MANY_REQ_FROM_SINGLE_IP,
	LOAN_REQUEST_ACCEPTED
	
}
