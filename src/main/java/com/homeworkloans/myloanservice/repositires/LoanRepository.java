package com.homeworkloans.myloanservice.repositires;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.homeworkloans.myloanservice.entities.Loan;

@Transactional
public interface LoanRepository extends JpaRepository<Loan, Long>{

}
