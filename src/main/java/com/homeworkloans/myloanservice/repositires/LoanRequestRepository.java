package com.homeworkloans.myloanservice.repositires;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.homeworkloans.myloanservice.entities.LoanRequest;

@Transactional
public interface LoanRequestRepository extends JpaRepository<LoanRequest, Long> {
}
