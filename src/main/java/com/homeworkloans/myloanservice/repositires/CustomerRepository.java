package com.homeworkloans.myloanservice.repositires;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.homeworkloans.myloanservice.entities.Customer;

@Transactional
public interface CustomerRepository extends JpaRepository<Customer, Long> {

}
